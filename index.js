const { spawn } = require('child_process');
const fs = require('fs');
// const mergeImg = require('merge-img');
const Jimp = require('jimp');
const path = require('path');

const images = process.argv.slice(2);
if (images.length === 0) console.log('please add images');

images.forEach((image) => {
  const name = image.substr(0, image.lastIndexOf('.'));
  const depthImage = `${name}-depth.jpg`;
  const combinedImage = `${name}-combined.jpg`;
  const exif = spawn('exiftool', ['-b', '-MPImage7', image]);
  exif.stdout.pipe(fs.createWriteStream(path.join(__dirname, depthImage)));
  exif.stderr.on('data', (data) => { console.error(`stderr: ${data}`); });
  exif.on('close', () => {
    Jimp.read(image, (err, img1) => {
      Jimp.read(depthImage, (err, img2) => {
        img1.contain(img1.bitmap.width * 2, img1.bitmap.height, Jimp.HORIZONTAL_ALIGN_LEFT, () => {
          img2.contain(img1.bitmap.width, img1.bitmap.height, Jimp.HORIZONTAL_ALIGN_LEFT, () => {
            img1.blit(img2, img1.bitmap.width / 2, 0, 0, 0, img1.bitmap.width, img1.bitmap.height, () => {
              img1.write(combinedImage, (err, res) => {
                console.log('done saving combined image');
              });
            });
          });
        });
      });
    });
  });
});
